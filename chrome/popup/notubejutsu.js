var restricted
var actionMode
var short
let newEntry = document.getElementById("newEntry")
let selectAction = document.getElementById("action")
let removeShort = document.getElementById("shortsRemoval")

removeShort.addEventListener("change", (event) =>{
    short = { hideshorts: removeShort.checked }
    chrome.storage.local.set(short)
});

selectAction.addEventListener("change", (event) => {
    actionMode = { action: event.target.value }
    chrome.storage.local.set(actionMode);
});

chrome.storage.local.get(['action'], function(actionM) {
    actionMode = actionM
    if (actionMode.action == undefined){
        actionMode.action = "blur";
        chrome.storage.local.set(actionMode);
    } else {
        selectAction.value = actionMode.action
    }
})

chrome.storage.local.get(['hideshorts'], function(shorts){ 
    short = shorts
    if (short.hideshorts == undefined){
        removeShort.checked = false
        short.hideshorts = false
        chrome.storage.local.set(short)
    }
    else 
        removeShort.checked = short.hideshorts
})

chrome.storage.local.get(['values'], function(res){ 
    restricted = res 
    if ( res.values != undefined ){
        res.values.forEach((ban)=>{
            createItemBtn(ban)
        })
    }
})

newEntry.addEventListener("keyup", ({key})=>{
    if (key === "Enter") {
        if( restricted.values == undefined )
            restricted.values = [] 
        restricted.values.push(newEntry.value)
        chrome.storage.local.set(restricted)
        createItemBtn(newEntry.value)
        newEntry.value="";
    }
});

//<div class="button">Manage</div>

function removeBan(toRemove){
    let indexTR = restricted.values.indexOf(toRemove)
    restricted.values.splice(indexTR,1);
    chrome.storage.local.set(restricted)
    document.getElementById("banned").innerHTML = ""
    restricted.values.forEach((ban)=>{
        createItemBtn(ban)
    })
}

function createItemBtn(content){
    var button = document.createElement('button');
    button.innerHTML = "<i class=\"fa fa-close\"></i>"+ content 
    button.onclick = function(){removeBan(content)}
    document.getElementById("banned").appendChild(button)
}