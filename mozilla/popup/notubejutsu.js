var restricted
var actionMode
var short
let newEntry = document.getElementById("newEntry")
let selectAction = document.getElementById("action")
let removeShort = document.getElementById("shortsRemoval")

removeShort.addEventListener("change", (event) =>{
    short = { hideshorts: removeShort.checked }
    browser.storage.local.set(short)
});

selectAction.addEventListener("change", (event) => {
    actionMode = { action: event.target.value }
    browser.storage.local.set(actionMode);
});

browser.storage.local.get('action').then( (actionM) => { 
    actionMode = actionM
    if (actionMode.action == undefined){
        actionMode.action = "blur";
        browser.storage.local.set(actionMode);
    } else {
        selectAction.value = actionMode.action
    }
})

browser.storage.local.get('hideshorts').then( (shorts) => { 
    short = shorts
    if (short.hideshorts == undefined){
        removeShort.checked = false
        short.hideshorts = false
        browser.storage.local.set(short)
    }
    else 
        removeShort.checked = short.hideshorts
})

browser.storage.local.get('values').then( (res) => { 
    restricted = res 
    if ( res.values != undefined ){
        res.values.forEach((ban)=>{
            createItemBtn(ban)
        })
    }
})

newEntry.addEventListener("change", function(){
    if( restricted.values == undefined )
        restricted.values = [] 
    
    restricted.values.push(newEntry.value)
    browser.storage.local.set(restricted)
    createItemBtn(newEntry.value)
    newEntry.value="";
});

//<div class="button">Manage</div>

function removeBan(toRemove){
    let indexTR = restricted.values.indexOf(toRemove)
    restricted.values.splice(indexTR,1);
    browser.storage.local.set(restricted)
    document.getElementById("banned").innerHTML = ""
    restricted.values.forEach((ban)=>{
        createItemBtn(ban)
    })
}

function createItemBtn(content){
    var button = document.createElement('button');
    button.innerHTML = "<i class=\"fa fa-close\"></i>"+ content 
    button.onclick = function(){removeBan(content)}
    document.getElementById("banned").appendChild(button)
}