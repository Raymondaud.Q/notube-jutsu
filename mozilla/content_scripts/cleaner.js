var alreadyWorking = false;
var locationRef = window.location.href;

// function search_tubejutsu("ytd-video-renderer")
// function side_tubejutsu("ytd-compact-video-renderer")
const SIDE_VIDEO_ITEM = "ytd-compact-video-renderer"
const SEARCH_VIDEO_ITEM = "ytd-video-renderer"
const HOME_VIDEO_ITEM = "ytd-rich-item-renderer"
const SUBS_VIDEO_ITEM = "ytd-grid-video-renderer"

function tubejutsu(selectorArg){
  if ( ! alreadyWorking ){
    alreadyWorking = true
    browser.storage.local.get('hideshorts').then( (shorts) => {
      browser.storage.local.get('action').then( (actionMode) => { 
        browser.storage.local.get('values').then( (res) => {
          document.querySelectorAll(selectorArg).forEach(e => {
            e.style.filter="blur(0px)";
            e.style.display="initial"
            if ( actionMode.action != "nothing" ){
              if ( shorts.hideshorts==true && String(e.innerHTML).includes("/shorts/")){
                if (actionMode.action == "blur"){
                  e.style.display="initial"
                  e.style.filter="blur(30px)";
                }
                else if ( actionMode.action == "hide")
                  e.style.display="none"
              } else {
                  res.values.forEach((key)=>{
                  if( String(e.textContent).toUpperCase().includes(key.toUpperCase())){
                    if (actionMode.action == "blur"){
                      e.style.display="initial"
                      e.style.filter="blur(30px)";
                    }
                    else if ( actionMode.action == "hide")
                      e.style.display="none"
                  }
                }) 
              }
            }
          });
        })
      })
    })
    setTimeout(function(){ alreadyWorking = false}, 1000 )
  }
}

function applyRestriction(){
  if ( locationRef == "https://www.youtube.com/")
    tubejutsu(HOME_VIDEO_ITEM);
  else if ( locationRef == "https://www.youtube.com/feed/subscriptions" )
    tubejutsu(SUBS_VIDEO_ITEM)
  else if ( locationRef.includes("/results?search") )
    tubejutsu(SEARCH_VIDEO_ITEM);
  else if ( locationRef.includes("/watch?v=") )
    tubejutsu(SIDE_VIDEO_ITEM);
}

(function() {

  applyRestriction()
  document.addEventListener("mousemove", applyRestriction ) 
  document.addEventListener("scroll", applyRestriction )
  browser.storage.onChanged.addListener( 
    function(){ 
      alreadyWorking = false
      applyRestriction()
    }
  )

  setInterval(function(){
    newLocationRef = window.location.href;
    if ( locationRef != newLocationRef ) {
      alreadyWorking = false;
      locationRef = window.location.href;
      setTimeout( applyRestriction, 2000)
    }
  },1000)
})();
