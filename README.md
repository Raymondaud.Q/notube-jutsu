# NoTube-Jutsu

**Web Browser Extension** that **hide Youtube content** according to some **keywords**

## Firefox
[**Notube-Jutsu is available here for Firefox !**](https://addons.mozilla.org/fr/firefox/addon/notube-jutsu/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
![Presentation](https://gitlab.com/Raymondaud.Q/notube-jutsu/-/raw/main/releases/mozilla/notube-jutsu-1.png)

## Opera
[**Notube-Jutsu is available here for Opera !**](https://addons.opera.com/en/extensions/details/notube-jutsu/)
![Presentation](https://gitlab.com/Raymondaud.Q/notube-jutsu/-/raw/main/releases/chrome/opera_screenshot.png)

## Chrome
* Not available on Chrome Store because I don't want to pay $5 to register my google account !
* You can get it from Opera Addons Store